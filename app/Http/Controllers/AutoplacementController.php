<?php
namespace App\Http\Controllers;

use App\Seater;
use App\SeaterGroup;
use App\SeatingPlan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\SeatingPlacement;
use SVG\SVG;
use Mail;
use App\Mail\OrderShipped;

class AutoplacementController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $seatingPlans = SeatingPlan::where('user_id', auth()->id())->get();

        return view('autoplacement.index', compact('seatingPlans'));
    }

    public function show($id)
    {
        $seatingPlan = SeatingPlan::where('id', $id)->first();
        $i = 1;
        $seatersGroups = SeaterGroup::where('seating_plan_id', $seatingPlan->id)->get();
        $seaters = Seater::whereIn('seater_group_id', $seatersGroups->pluck('id'))->get();

        $file = $seatingPlan->svg;

        return view('autoplacement.show', compact('seatingPlan', 'seatersGroups', 'file', 'seaters', 'i'));
    }

    public function getSeatersByGroupId($idGroup) 
    {
        $seaters = Seater::where('seater_group_id', $idGroup)->get();
        
        return response()->json($seaters);
    }

    public function placement($id)
    {
        //function random :   
        $seatingPlan = SeatingPlan::where('id', $id)->first();

        $file = $seatingPlan->svg;

        //utilisation de php-svg pour transformé le string sgv en objet svg 
        $image = SVG::fromString($file);
        $doc = $image->getDocument();
        $svgTspan = $doc->getElementsByTagName('tspan');


        //récupérer tous les éléments et span qui contiennent les chiffres dans un tableau
        foreach ($svgTspan as $text) {
            $values[] = $text->getValue();
        }

        //shuffle les valeurs et les mettre dans un tableau
        shuffle($values);
        foreach ($values as $value) {
            $valueShuffle[] = $value;
        }

        //pousse les valeurs dans le svg
        $j = 0;

        foreach ($svgTspan as $value) {
            $value->setValue($valueShuffle[$j]);
            $j++;
        }

        //mettre les chiffre à une font size de 30px
        $svgText = $doc->getElementsByTagName('text');
        foreach ($svgText as $text) {
            $text->setStyle('font-size', '30px');
        }


        header('Content-Type: image/svg+xml');
        $stringImage = (string)$image;

        //savegarde des datas du placement
        $seatingPlacement = new SeatingPlacement(request()->only(['seatingPlanId', 'seaterGroupId','placementImage']));
        $seatingPlacement->user_id = auth()->id();
        $seatingPlacement->seating_plan_id = $id;
        $seatingPlacement->seater_group_id = request('group_id');
        $seatingPlacement->save();


        //header('Content-Type: image/svg+xml');
        $randomizedPlacementImage = (string)$image;
        
//////////////////////////////////////////////////////////////////////////////////////////
        //savegarde des datas du placement
        //$seatingPlacement = new SeatingPlacement(request()->only(['seatingPlanId', 'seaterGroupId','placementImage']));
        //$seatingPlacement->user_id = auth()->id();
        //$seatingPlacement->seating_plan_id = $seatingPlan->id;//
        //$seatingPlacement->seater_group_id = request('groupe_id');
        //$seatingPlacement->save();
//////////////////////////////////////////////////////////////////////////////////////////        

        //sauvegarde de l image        
        //$randomPlacementSvg = $seatingPlacement->id.'-placement.svg';
        //Storage::put($randomPlacementSvg, $randomizedPlacementImage);
        //$seatingPlacement->svg = $randomPlacementSvg;
        //$seatingPlacement->save();

        // //test fonction d'envoie d'email

        // $MailName = "loic";
        // $message = "bla bla bla";
        // $data = [
        //     'firstname' => $MailName,
        //     'message' => $message
        // ];

        // Mail::to('loicneyron@gmail.com')->send(new OrderShipped());

        // $rasterImage = $image->toRasterImage(2000, 1000);

        return redirect()->back()
            ->with("randomizedPlacementImage", $randomizedPlacementImage)
            ->with('group_id', request('group_id'));
    }
}