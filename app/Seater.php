<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Seater extends Model
{
     /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'seaters';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'firstname','lastname','email', 'seater_group_id'
    ];

    /**
     * Relation de type n:n
     * 
     * Get the seaterGroup that belongs to the seater.
     */
    public function seaterGroup()
    {
        return $this->belongsToMany('App\SeaterGroup');
    }

    /**
     * Relation de type n:n
     * 
     * Get the seaterPlacement that belongs to the seater.
     */
    public function seaterPlacement()
    {
        return $this->belongsToMany('App\SeaterPlacement');
    }
}
