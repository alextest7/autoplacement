<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SeaterGroup extends Model
{
    /**
    * The table associated with the model.
    *
    * @var string
    */
    protected $table = 'seater_groups';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'user_id', 'seating_plan_id', 'parent_id'
    ];

    /**
     * Relation de type 1:n
     * 
     * Get the user that owns the seaterGroup.
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * Relation de type 1:n
     * 
     * Get the seater that belongs to the seaterGroup.
     */
    public function seaters()
    {
        return $this->hasMany('App\Seater');
    }

    /**
     * Relation de type 1:n
     *
     * Get the seater that belongs to the seaterGroup.
     */
    public function subgroups()
    {
        return $this->hasMany('App\SeaterGroup', 'parent_id' ,'id');
    }

    /**
     * Relation de type 1:n
     * 
     * Get the seatingPlacements from seaterGroup.
     */
    public function seatingPlacement()
    {
        return $this->hasMany('App\SeatingPlacement');
    }
}
