<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SeaterPlacement extends Model
{
     /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'seater_placements';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'seating_placement_id', 'seater_id', 'position'
    ];

    /**
     * Relation de type n:n
     * 
     * Get the seater that belongs to the seaterPlacement.
     */
    public function seater()
    {
        return $this->belongsToMany('App\Seater');
    }

    /**
     * Relation de type n:n
     * 
     * Get the seatingPlacement that belongs to the seaterPlacement.
     */
    public function seatingPlacement()
    {
        return $this->belongsToMany('App\SeatingPlacement');
    }
}
