<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SeatingPlacement extends Model
{
    /**
    * The table associated with the model.
    *
    * @var string
    */
    protected $table = 'seating_placements';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'seating_plan_id', 'seater_group_id'
    ];

    /**
     * Relation de type n:n
     * 
     * Get the seaterPlacement that belongs to the seatingPlacement.
     */
    public function seaterPlacement()
    {
        return $this->belongsToMany('App\SeaterPlacement');
    }

     /**
     * Relation de type 1:n
     * 
     * Get the user that owns the seatingPlacement.
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

     /**
     * Relation de type 1:n
     * 
     * Get the seatingPlan that owns the seatingPlacement.
     */
    public function seatingPlan()
    {
        return $this->belongsTo('App\SeatingPlan');
    }

     /**
     * Relation de type 1:n
     * 
     * Get the seaterGroup that owns the seatingPlacement.
     */
    public function seaterGroup()
    {
        return $this->belongsTo('App\SeaterGroup');
    }
}