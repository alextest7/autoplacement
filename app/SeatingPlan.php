<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SeatingPlan extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'seating_plans';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'seater_group_id', 'json', 'svg', 'title', 'thumbnail'
    ];

    /**
     * Relation de type 1:n
     * 
     * Get the user that owns the seatingPlan.
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * Relation de type 1:n
     * 
     * Get the seatingPlacements for the seatingPlan.
     */
    public function seatingPlacements()
    {
        return $this->hasMany('App\SeatingPlacement');
    }
}
