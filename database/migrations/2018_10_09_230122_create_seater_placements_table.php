<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSeaterPlacementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('seater_placements', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('seating_placement_id')->unsigned();
            $table->integer('seater_id')->unsigned();
            $table->integer('position');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('seater_placements');
    }
}
