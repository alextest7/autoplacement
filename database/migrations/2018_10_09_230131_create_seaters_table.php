<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSeatersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('seaters', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('seater_group_id')->unsigned();
            $table->string('firstname',255)->nullable();
            $table->string('lastname',255)->nullable();
            $table->string('email',255)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('seaters');
    }
}
