<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateForeignKeys extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('seating_plans', function(Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users')
                ->onDelete('cascade')
                ->onUpdate('restrict');
        });
        Schema::table('seater_groups', function(Blueprint $table) {
            $table->foreign('user_id')->references('id')->on('users')
                ->onDelete('cascade')
                ->onUpdate('restrict');

            $table->foreign('parent_id')->references('id')->on('seater_groups')
                ->onDelete('cascade')
                ->onUpdate('restrict');

            $table->foreign('seating_plan_id')->references('id')->on('seating_plans')
                ->onDelete('cascade')
                ->onUpdate('restrict');
        });
        Schema::table('seaters', function(Blueprint $table) {
            $table->foreign('seater_group_id')->references('id')->on('seater_groups')
            ->onDelete('cascade')
            ->onUpdate('restrict');
        });
        Schema::table('seating_placements', function(Blueprint $table) {
            $table->foreign('seater_group_id')->references('id')->on('seater_groups')
            ->onDelete('cascade')
            ->onUpdate('restrict');
            $table->foreign('seating_plan_id')->references('id')->on('seating_plans')
            ->onDelete('cascade')
            ->onUpdate('restrict');
            $table->foreign('user_id')->references('id')->on('users')
            ->onDelete('cascade')
            ->onUpdate('restrict');
        });
        Schema::table('seater_placements', function(Blueprint $table) {
            $table->foreign('seater_id')->references('id')->on('seaters')
            ->onDelete('cascade')
            ->onUpdate('restrict');
            $table->foreign('seating_placement_id')->references('id')->on('seating_placements')
            ->onDelete('cascade')
            ->onUpdate('restrict');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
