
# Autoplacement

- Placer aléatoirement des élèves sur une disposition de table dessinée.
- Générer automatiquement des placements tous les jours ou toutes les semaines.
- Gestion de règles de placement, par exemple "X ne doit pas être assis au fond"

## Installation 

- `git clone https://gitlab.com/simplon-roanne/autoplacement`

### Dépendances PHP et JS
- `composer update`
- `yarn install`

### Config du .env

- Copier `.env.example` vers `.env` et le connecter à une base de données vide
- `php artisan key:generate`
- ``php artisan migrate:fresh``

## Développement

### Après chaque git pull

- `composer update`
- `yarn install`
- `php artisan migrate:fresh`
- `npm run dev`

### Mettre à jour la base de données

`php artisan migrate:fresh` régénère la base de données. A exécuter après avoir modifié une migration.

### Compiler les fichiers JS et SASS

`npm run watch` est la seule commande à utiliser quand on fait du JS ou du CSS. 
Ca compile tous les fichiers dans le dossier `resources `et ca génère des fichiers minifiés et compilés dans le dossier `public`

