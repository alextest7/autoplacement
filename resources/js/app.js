
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap.js');

window.Vue = require('vue');

$(window).scroll(function () {
    var sc = $(window).scrollTop()
    if (sc > 800) {
        $(".rgba-blue-strong").addClass("navbar-scroll")
    }
    else {
        $(".rgba-blue-strong").removeClass("navbar-scroll")
    }
});