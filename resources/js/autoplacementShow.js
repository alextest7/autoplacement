
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');
require('fabric');
window.Vue = require('vue');

fabric.Object.prototype.set({
    transparentCorners: false,
    cornerColor: 'rgba(102,153,255,0.5)',
    cornerSize: 12,
    padding: 5
});

var canvas = window._canvas = new fabric.Canvas('canvasShow');


// canvas.loadFromJSON(json, canvas.renderAll.bind(canvas), function(o, object) {
//     fabric.log(o, object);
// });

