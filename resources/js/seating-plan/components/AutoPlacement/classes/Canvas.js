import {SeatDrawing, TextDrawing} from "./Drawings";
import {FabricShape, SeatShape} from "./Shapes";
import {fabric} from 'fabric'

const fabricProxiedCanvasEvents = [
    'object:modified',
    'object:removed',
    'selection:created',
    'selection:cleared',
];

export default class {
    constructor(canvasElementId) {
        this.canvas = new fabric.Canvas(canvasElementId, {
            selection: false,
            hoverCursor: "normal"
        });
        //this.canvas.freeDrawingCursor = 'crosshair';

        this.activeShapeClass = null;
        this.canDraw = false;
        this.events = {};

        this.setupEvents();
    }

    initFromJSON(json, callback) {
        //  https://stackoverflow.com/questions/28877001/canvas-only-renders-custom-webfont-on-click-event-with-fabric-js
        // Browser won't load custom fonts on demand, unless the page is loaded
        // AND the font family has been used before
        window.addEventListener('load', _ => {
            this.loadFromJSON(json, _ => {
                this.reorderSeats();
                this.toggleAllSelectable(false);
                callback();
            }, (_, object) => {
                const identifiedShapeTypes = {
                    'rect': FabricShape,
                    'group': SeatShape,
                    'text': FabricShape,
                };
                let shape = new (identifiedShapeTypes[object.type])(object);
                this.addShape(shape);
            });
        });
    }

    loadFromJSON(json, callback, objectCallback = null) {
        this.canvas.clear();
        this.canvas.loadFromJSON(json, () => {
            this.refresh();
            callback();
        }, objectCallback);
    }

    setupEvents() {
        let isDrawing = false;
        let currentShapeBeingDrawn = null;

        this.canvas.on('mouse:down', (o) => {
            let object = this.canvas.getActiveObject();

            if ((object && object === this.getLastObject()) || !this.canDraw) {
                return;
            }

            if (object) {
                this.canvas.discardActiveObject();
            }

            isDrawing = true;
        });

        this.canvas.on('mouse:move', (o) => {
            if (!isDrawing || !this.canDraw) return;

            if (!currentShapeBeingDrawn) {
                currentShapeBeingDrawn = new (this.activeShapeClass)(this.canvas.getPointer(o.e));
                this.addShape(currentShapeBeingDrawn);

                this.triggerEvent('drawing:start', currentShapeBeingDrawn);
            }

            currentShapeBeingDrawn.updateDimensions(this.canvas.getPointer(o.e));

            // This line refresh the canvas on every pixel movement
            this.refresh();
        });

        this.canvas.on('mouse:up', (o) => {
            if (!isDrawing || !this.canDraw) {
                return;
            }

            isDrawing = false;

            if (currentShapeBeingDrawn) {
                currentShapeBeingDrawn = null;

                this.toggleAllSelectable(false);

                // Put element in editable mode
                this.getLastObject().setCoords();
                this.getLastObject().set({selectable: true});
                this.canvas.setActiveObject(this.getLastObject());
                this.canvas.renderAll();

                this.triggerEvent('drawing:end')
            }
        });

        // Proxy to fabric js native events
        fabricProxiedCanvasEvents.forEach(eventName => {
            this.canvas.on(eventName, _ => {
                this.triggerEvent(eventName);
            })
        });
    }

    on(eventName, callback) {
        if (!Array.isArray(this.events[eventName])) {
            this.events[eventName] = [];
        }
        this.events[eventName].push(callback);
    }


    forEachShapeOfType(types, f) {
        if (!Array.isArray(types)) {
            types = [types];
        }
        this.canvas.getObjects().forEach(object => {
            types.forEach(type => {
                if (object.shape instanceof type) {
                    f(object.shape);
                }
            });
        });
        this.canvas.renderAll();
    }

    countSeats() {
        let seatsCount = 0;
        this.forEachSeats(_ => seatsCount++);
        return seatsCount;
    }

    reorderSeats() {
        let seatsCount = 0;
        this.forEachSeats((object) => {
            seatsCount++;
            object.getObjects()[1].set('text', seatsCount.toString());
        });
    }

    forEachSeats(callback) {
        this.canvas.getObjects().forEach((object) => {
            if (object.getObjects && object.getObjects() && object.getObjects()[1] instanceof fabric.Text) {
                callback(object);
            }
        });
    }

    addShape(shape) {
        shape.addToCanvas(this.canvas);

        if (shape instanceof SeatDrawing) {
            shape.setText(this.countSeats());
        }
    }

    getLastObject() {
        return this.canvas ? this.canvas.getObjects()[this.canvas.getObjects().length - 1] : null
    }

    updateDimensions(width, height) {
        this.canvas.setWidth(width);
        this.canvas.setHeight(height);
    }

    removeActiveObject() {
        let activeObject = this.canvas.getActiveObject();
        if (!activeObject) return;

        if (activeObject.getObjects) {
            activeObject.getObjects().forEach((object) => {
                this.canvas.remove(object);
            });
            this.canvas.discardActiveObject().renderAll();
        }
        this.canvas.remove(activeObject);

        this.reorderSeats();
    }

    discardActiveObject() {
        this.canvas.discardActiveObject().renderAll();
    }

    setActiveObject(object) {
        this.canvas.setActiveObject(object).requestRenderAll();
    }

    toggleSelectionMode(enabled) {
        this.toggleDrawing(!enabled);

        if (!enabled) {
            this.discardActiveObject()
        }

        this.canvas.set({
            selection: enabled
        });

        this.toggleAllSelectable(enabled)
    }

    toggleAllSelectable(enabled) {
        this.canvas.getObjects().forEach((object) => {
            object.set({
                selectable: enabled
            });
        });
    }

    getActiveObject() {
        return this.canvas.getActiveObject();
    }

    refresh() {
        this.canvas.renderAll()
    }

    getPng(sizeMultiplier) {
        return this.canvas.toDataURL({
            format: 'png',
            multiplier: sizeMultiplier
        });
    }

    getSvg() {
        return this.canvas.toSVG();
    }

    triggerEvent(eventName, payload = null) {
        if (!Array.isArray(this.events[eventName])) {
            return;
        }

        this.events[eventName].forEach(event => {
            if (payload) {
                event(payload);
            }
            else {
                event();
            }
        });
    }

    setActiveShapeClass(newClass) {
        this.activeShapeClass = newClass;
    }

    toggleDrawing(enabled) {
        this.canDraw = enabled;
    }

    getState() {
        return JSON.stringify(this.canvas);
    }
}