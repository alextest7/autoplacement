/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('../bootstrap');
window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page.
 */
import store from './store'

Vue.component('auto-placement', require('./components/AutoPlacement'));
Vue.component('seaters-manager', require('./components/SeatersManager'));
Vue.component('seaters-importer', require('./components/SeatersImporter'));

let createSeatingPlanVue = new Vue({
    store,
    el: "#createSeatingPlanVue",
    data() {
        return {
            importedSeaters : null
        }
    },
    methods: {
        sendForm() {
            this.$refs.editor.exportPng();
            this.$refs.editor.exportSvg();
            // Timeout needed for Vue to tick again
            setTimeout(function () {
                document.querySelector('form').submit();
            }, 0);
        },
        setSeaters(seaters) {
            this.importedSeaters = seaters;
        }
    }
});
