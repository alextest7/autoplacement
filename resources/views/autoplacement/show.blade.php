@extends('layouts.app')

@section('content')
    <div class="container mt-100 mb-50" id="showPlacement">
        <h1 class="display-4">Placement automatique</h1>
        <p class="lead">Génerez autant de plan de salle automatiques que vous le souhaitez, et diffusez les par Email,
            SMS, sur Discord ou avec un lien.</p>

        <form method="post" class="form mb-50" action="{{route('autoplacement.random', $seatingPlan->id)}}">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            {{-- canva ou png du plan de table --}}
            <div id="canvaShow" class="row ">
                <div class="col-8">

                    @if (session('randomizedPlacementImage'))
                        {!! session('randomizedPlacementImage') !!}
                    @else
                        {!! $file !!}
                    @endif

                    {{-- <input type="hidden" id="imgInput" name="placementSVGImage" value="{{$file}}"> --}}
                    <input type="hidden" id="image" name="placeImage">
                </div>
            </div>

            {{-- boutons d'envoi de l'autoplacement par email/sms et enregistrement en bdd de l'iteration --}}

            <seater-group-selector :seaters-init='{!! json_encode($seaters) !!}'
                                   :seater-groups='{!! json_encode($seatersGroups) !!}'
                                    group-id="{{session('group_id')}}"></seater-group-selector>
            {{-- bouton de déclenchement du random --}}


            <div class="float-right">
                @if (session('randomizedPlacementImage'))
                    <button type="submit" id="" class="btn btn-secondary toto"><i class="fa fa-envelope"></i> Envoyer
                        par email
                    </button>
                    <button type="submit" class="btn btn-secondary toto"><i class="fa fa-envelope"></i> Envoyer par SMS
                    </button>
                    <button type="submit" class="btn btn-secondary toto"><i class="fa fa-comment"></i> Envoyer sur
                        Discord
                    </button>
                    <button type="submit" class="btn btn-secondary toto"><i class="fa fa-bell"></i> Envoyer par
                        notification
                    </button>
                @endif
                <button class="btn btn-primary" @click="generateIfHasGroup"><i class="fa fa-random"></i> Générer
                </button>
            </div>
            <div class="clearfix"></div>
        </form>
    </div>
@endsection

@section('scripts')
    <script src="{{ url("/js/autoplacement.show.js") }}"></script>
    <script>
        /*var canvas = documenquerySelectort.getElementById("imgInput");
           var img    = this.canvas.toDataURL({
                   format: 'png',
                   multiplier: 1
               })*/
    </script>

@endsection