
    <!-- Modal Register-->
    <div class="modal fade" id="elegantModalForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <!--Content-->
            <div class="modal-content form-elegant">
                <!--Header-->
                <div class="modal-header text-center">
                    <h3 class="modal-title w-100 dark-grey-text font-weight-bold my-3" id="myModalLabel"><strong>Créer votre compte</strong></h3>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <!--Body-->
                <div class="modal-body mx-4">
                    <!--Body-->
                    <form method="POST" action="{{ route('register') }}">
                        @csrf
                        <div class="md-form mb-5">
                            <input id="name" type="text" class="form-control validate{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>
                            <label for="name" data-error="wrong" data-success="right">{{ __('Nom') }}</label>
                            @if ($errors->has('name'))
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                            @endif
                        </div>
                        <div class="md-form mb-5">
                            <input id="email" type="email" class="form-control validate{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>
                            <label for="email" data-error="wrong" data-success="right">{{ __('Email') }}</label>
                            @if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                            @endif
                        </div>
                        <div class="md-form mb-5">
                            <input type="password" id="password" class="form-control validate{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required">
                            <label for="password" data-error="wrong" data-success="right">{{ __('Mot de passe') }}</label>
                            @if ($errors->has('password'))
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                            @endif
                        </div>
                        <div class="md-form mb-3">
                            <input id="password-confirm" type="password" class="form-control validate" name="password_confirmation" required>
                            <label for="password-confirm" data-error="wrong" data-success="right">{{ __('Confirmer mot de passe') }}</label>
                            <p class="font-small blue-text d-flex justify-content-end"><a href="#" class="blue-text ml-1">
                                    Mot de passe oublié?</a></p> {{--modif a faire ici couleur texte--}}
                        </div>
                        <div class="text-center mb-3">
                            <button type="submit" class="btn logoF login-register waves-effect btn-block sign-in z-depth-1">
                                {{ __("S'enregistrer") }}
                            </button>
                        </div>
                        <p class="font-small dark-grey-text text-right d-flex justify-content-center mb-3 pt-2"> ou connectez-vous avec:</p>
                        <div class="row my-3 d-flex justify-content-center">
                            <!--Facebook-->
                            <button type="button" class="btn btn-white sign-in btn-rounded mr-md-3 z-depth-1a"><i class="fa fa-facebook text-center"></i></button>
                            <!--Twitter-->
                            <button type="button" class="btn btn-white sign-in logoF btn-rounded mr-md-3 z-depth-1a"><i class="fa fa-twitter"></i></button>
                            <!--Google +-->
                            <button type="button" class="btn btn-white sign-in btn-rounded z-depth-1a"><i class="fa fa-google-plus"></i></button>
                        </div>
                    </form>
                </div>
                <!--Footer modal-->
                <div class="modal-footer mx-5 pt-3 mb-1">
                    <p class="font-small grey-text d-flex justify-content-end">Déjà membre? <a href="#" data-toggle="modal" data-target="#elegantModalForm2" class="blue-text ml-1">Se connecter</a></p>
                </div>
            </div>
            <!--/.Content-->
        </div>
    </div>
    <!--End Modal Register-->
    <!-- Modal login-->
    <div class="modal fade" id="elegantModalForm2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <!--Content-->
            <div class="modal-content form-elegant">
                <!--Header-->
                <div class="modal-header text-center">
                    <h3 class="modal-title w-100 dark-grey-text font-weight-bold my-3" id="myModalLabel"><strong>Connectez-vous à votre compte</strong></h3>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <!--Body-->
                <div class="modal-body mx-4">
                    <!--Body-->
                    <form method="POST" action="{{ route('login') }}">
                        @csrf
                        <div class="md-form mb-5">
                            <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>
                            <label for="email" data-error="wrong" data-success="right">{{ __('Email') }}</label>
                            @if ($errors->has('email'))
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                            @endif
                        </div>
                        <div class="md-form mb-5">
                            <input type="password" id="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required">
                            <label for="password">{{ __('Mot de passe') }}</label>
                            @if ($errors->has('password'))
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                            @endif
                        </div>
                        <div class="form-group row">
                            <div class="col-md-6">
                                <div class="form-check">
                                    <input type="checkbox" class="custom-control-input" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                                    <label class="custom-control-label" for="remember">{{ __('Se souvenir de moi') }}</label>
                                </div>
                            </div>
                        </div>
                        <div class="md-form mb-3">
                            <p class="font-small blue-text d-flex justify-content-end"><a href="#" class="blue-text ml-1">Mot de passe oublié?</a></p>
                        </div>
                        <div class="text-center mb-3">
                            <button type="submit" class="btn logoF login-register waves-effect btn-block sign-in z-depth-1">
                                {{ __('Se connecter') }}
                            </button>
                        </div>
                        <p class="font-small dark-grey-text text-right d-flex justify-content-center mb-3 pt-2"> ou connectez-vous avec:</p>
                        <div class="row my-3 d-flex justify-content-center">
                            <!--Facebook-->
                            <button type="button" class="btn btn-white sign-in btn-rounded mr-md-3 z-depth-1a"><i class="fa fa-facebook text-center"></i></button>
                            <!--Twitter-->
                            <button type="button" class="btn btn-white sign-in logoF btn-rounded mr-md-3 z-depth-1a"><i class="fa fa-twitter"></i></button>
                            <!--Google +-->
                            <button type="button" class="btn btn-white sign-in btn-rounded z-depth-1a"><i class="fa fa-google-plus"></i></button>
                        </div>
                    </form>
                </div>
                <!--Footer-->
                <div class="modal-footer mx-5 pt-3 mb-1">
                    <p class="font-small grey-text d-flex justify-content-end">Pas encore membre? <a href="#" data-toggle="modal" data-target="#elegantModalForm" class="blue-text ml-1">S'inscrire</a></p>
                </div>
            </div>
            <!--/.Content-->
        </div>
    </div>
<!--End Modal login-->